//
//  SaveScreenViewController.swift
//  ScannerAcorys
//
//  Created by Corify Care on 13/5/24.
//

import Foundation
import UIKit

protocol RestartScreenDelegate: AnyObject {
  func QRViewNavigate()
  func viewControllerNavigate()
}

class RestartScreenViewController: UIViewController {
  weak var delegate: RestartScreenDelegate?
  
  @IBOutlet weak var newQRScanButton: UIButton!
  @IBOutlet weak var sameTorsoScanButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  @IBAction func newQRScanButtonPressed(_ sender: UIButton) {
    dismiss(animated: true) {
      self.delegate!.QRViewNavigate()
    }
  }
  
  @IBAction func sameTorsoScanButtonPressed(_ sender: UIButton) {
    dismiss(animated: true) {
      self.delegate!.viewControllerNavigate()
    }
  }
}
