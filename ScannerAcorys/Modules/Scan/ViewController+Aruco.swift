//
//  ViewController+Aruco.swift
//  ScannerAcorys
//
//  Created by Corify Care on 8/5/24.
//

import AVFoundation
import Structure
import UIKit


extension ViewController {
  func prepareForNextArucos() {
      for imageName in imageViewsDictionary.keys {
          guard let imageView = imageViewsDictionary[imageName], let circleView = circleViewsDictionary[imageName] else {
              continue
          }
          
          if !imageView.isHidden {
              imageView.isHidden = true
              circleView.isHidden = false
          }
      }
  }
  
  func resetDetectArucos() {
    splitSection = 1
    isSplitScan = true
    numberSplitImage.image = UIImage(systemName: "1.circle.fill")
    
    arucosProgressView.progress = 0
    frProgressBar.progress = 0.0
    brProgressBar.progress = 0.0
    flProgressBar.progress = 0.0
    blProgressBar.progress = 0.0
    
    let greenColor = UIColor.systemGreen
    var hue: CGFloat = 0.0
    var saturation: CGFloat = 0.0
    var brightness: CGFloat = 0.0
    var alpha: CGFloat = 0.0
    greenColor.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
    saturation = 0.2
    brightness = 0.5
    let hsbColor = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
    
    frProgressBar.progressTintColor = hsbColor
    brProgressBar.progressTintColor = hsbColor
    flProgressBar.progressTintColor = hsbColor
    blProgressBar.progressTintColor = hsbColor
    
    numberAucoDetectedLabel.text = "0"
    currentArucoDetected = []
    currentArucoDetectedFR = []
    currentArucoDetectedFL = []
    currentArucoDetectedBR = []
    currentArucoDetectedBL = []
    arucoDetectedParts = []
    arucoDetected = []
    
    for imageName in imageViewsDictionary.keys {
      if let imageView = imageViewsDictionary[imageName] {
        let newTintColor = UIColor(red: 0.498, green: 1, blue: 0.553, alpha: 1)
        
        imageView.alpha = 0.5
        imageView.isHidden = true
        imageView.tintColor = newTintColor
        imageView.image = UIImage(systemName: "circle.fill")
      }
    }
    
    for imageName in circleViewsDictionary.keys {
      if let imageView = circleViewsDictionary[imageName] {
        imageView.isHidden = true
      }
    }
  }
  
  func addAruco(_ arucoInList: Int) {
    var found = false
    
    for arrayObject in arucoDetected {
      if (arrayObject as AnyObject).isEqual(arucoInList) {
        found = true
        break
      }
    }
    
    if !found {
      arucoDetected.append(arucoInList)
    }
    
    let detectedPercentage = Float(arucoDetected.count) / 128.0
    self.arucosProgressView.progress = detectedPercentage
    
    let numeroStr = String(arucoDetected.count)
    self.numberAucoDetectedLabel.text = numeroStr
  }
  
  func registerAruco(_ image: UIImage) {
    let arucoList = ArucoCV.detectArucos(image)
    
    for arucoNumber in arucoList {
      let arucoID = arucoNumber as! Int
      
      print("ID: \(arucoID)")
      
      var encontrado = false
      
      for numero in currentArucoDetected {
        if numero as! Int == arucoID {
          encontrado = true
          break
        }
      }
      
      if !encontrado {
        self.currentArucoDetected.append(arucoNumber)
        
        if arucoID >= 35 && arucoID <= 69 {
          self.currentArucoDetectedFR.append(arucoNumber)
          let detectedPercentage = Float(self.currentArucoDetectedFR.count) / 32.0
          self.frProgressBar.progress = detectedPercentage
          
          var hue: CGFloat = 0, saturation: CGFloat = 0, brightness: CGFloat = 0, alpha: CGFloat = 0
          self.frProgressBar.progressTintColor?.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
          
          brightness += 0.03
          brightness = min(1.0, brightness)
          brightness = max(0.0, brightness)
          
          saturation += 0.03
          saturation = min(1.0, saturation)
          saturation = max(0.0, saturation)
          
          hue += 0.005
          hue = min(1.0, hue)
          hue = max(0.0, hue)
          
          let hsbColor = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
          self.frProgressBar.progressTintColor = hsbColor
        }
        
        if arucoID >= 0 && arucoID <= 34 {
          self.currentArucoDetectedFL.append(arucoNumber)
          let detectedPercentage = Float(self.currentArucoDetectedFL.count) / 32.0
          self.flProgressBar.progress = detectedPercentage
          
          var hue: CGFloat = 0, saturation: CGFloat = 0, brightness: CGFloat = 0, alpha: CGFloat = 0
          self.flProgressBar.progressTintColor?.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
          
          brightness += 0.03
          brightness = min(1.0, brightness)
          brightness = max(0.0, brightness)
          
          saturation += 0.03
          saturation = min(1.0, saturation)
          saturation = max(0.0, saturation)
          
          hue += 0.005
          hue = min(1.0, hue)
          hue = max(0.0, hue)
          
          let hsbColor = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
          self.flProgressBar.progressTintColor = hsbColor
        }
        
        if arucoID >= 70 && arucoID <= 98 {
          self.currentArucoDetectedBR.append(arucoNumber)
          let detectedPercentage = Float(self.currentArucoDetectedBR.count) / 32.0
          self.brProgressBar.progress = detectedPercentage
          
          var hue: CGFloat = 0, saturation: CGFloat = 0, brightness: CGFloat = 0, alpha: CGFloat = 0
          self.brProgressBar.progressTintColor?.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
          
          brightness += 0.03
          brightness = min(1.0, brightness)
          brightness = max(0.0, brightness)
          
          saturation += 0.03
          saturation = min(1.0, saturation)
          saturation = max(0.0, saturation)
          
          hue += 0.005
          hue = min(1.0, hue)
          hue = max(0.0, hue)
          
          let hsbColor = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
          self.brProgressBar.progressTintColor = hsbColor
        }
        
        if arucoID >= 99 && arucoID <= 127 {
          self.currentArucoDetectedBL.append(arucoNumber)
          let detectedPercentage = Float(self.currentArucoDetectedBL.count) / 32.0
          self.blProgressBar.progress = detectedPercentage
          
          var hue: CGFloat = 0, saturation: CGFloat = 0, brightness: CGFloat = 0, alpha: CGFloat = 0
          self.blProgressBar.progressTintColor?.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
          
          brightness += 0.03
          brightness = min(1.0, brightness)
          brightness = max(0.0, brightness)
          
          saturation += 0.03
          saturation = min(1.0, saturation)
          saturation = max(0.0, saturation)
          
          hue += 0.005
          hue = min(1.0, hue)
          hue = max(0.0, hue)
          
          let hsbColor = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
          self.blProgressBar.progressTintColor = hsbColor
        }
      }
      
      addAruco(arucoNumber as! Int)
      
      for (imageName, imageView) in imageViewsDictionary {
        guard let imageID = Int(imageName) else {
          print("No se encontró un número en el nombre del componente.")
          continue
        }
        
        if imageID == arucoID {
          guard let existingTintColor = imageView.tintColor else { continue }
          
          var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
          existingTintColor.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
          
          blue += 0.1
          blue = min(1.0, blue)
          blue = max(0.0, blue)
          
          let newTintColor = UIColor(red: red, green: green, blue: blue, alpha: 1)
          
          imageView.tintColor = newTintColor
          imageView.isHidden = false
          imageView.alpha += 0.1
          
          let animation_height = CAKeyframeAnimation()
          animation_height.keyPath = "bounds.size.height"
          animation_height.values = [0, 10, -10, 10, 0]
          animation_height.keyTimes = [0 as NSNumber, NSNumber(value: 1 / 6.0), NSNumber(value: 3 / 6.0), NSNumber(value: 5 / 6.0), 1 as NSNumber]
          animation_height.duration = 0.4
          animation_height.isAdditive = true
          
          imageView.layer.add(animation_height, forKey: "shake_height")
          
          let animation_width = CAKeyframeAnimation()
          animation_width.keyPath = "bounds.size.width"
          animation_width.values = [0, 10, -10, 10, 0]
          animation_width.keyTimes = [0 as NSNumber, NSNumber(value: 1 / 6.0), NSNumber(value: 3 / 6.0), NSNumber(value: 5 / 6.0), 1 as NSNumber]
          animation_width.duration = 0.4
          animation_width.isAdditive = true
          
          imageView.layer.add(animation_width, forKey: "shake_width")
        }
      }
      
      for currentAruco in currentArucoDetected {
        for listAruco in arucoDetectedParts {
          if let listArucoDict = listAruco as? [AnyHashable: Any] {
            for (_, arucoInList) in listArucoDict {
              if let currentAruco = currentAruco as? NSObject, let arucoInList = arucoInList as? NSObject {
                if currentAruco.isEqual(arucoInList) {
                  for (imageName, imageView) in self.imageViewsDictionary {
                    guard let imageID = Int(imageName) else { continue }
                    if imageID == arucoID {
                      imageView.isHidden = false
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  func setupArucos() {
    warningMessageView.layer.cornerRadius = 10.0
    warningMessageView.layer.masksToBounds = true
    
    detectedArucoPercentageView.layer.cornerRadius = 10.0
    detectedArucoPercentageView.layer.masksToBounds = true
    
    splitSection = 1
    arucoDetectedParts = []
    arucoDetected = []
    
    imageViewsDictionary = [
      ///FR
      "65": self.Image65,
      "66": self.Image66,
      "67": self.Image67,
      "68": self.Image68,
      "69": self.Image69,
      "60": self.Image60,
      "61": self.Image61,
      "62": self.Image62,
      "63": self.Image63,
      "64": self.Image64,
      "51": self.Image51,
      "52": self.Image52,
      "53": self.Image53,
      "54": self.Image54,
      "55": self.Image55,
      "56": self.Image56,
      "57": self.Image57,
      "58": self.Image58,
      "59": self.Image59,
      "43": self.Image43,
      "44": self.Image44,
      "45": self.Image45,
      "46": self.Image46,
      "47": self.Image47,
      "48": self.Image48,
      "49": self.Image49,
      "50": self.Image50,
      "35": self.Image35,
      "36": self.Image36,
      "37": self.Image37,
      "38": self.Image38,
      "39": self.Image39,
      "40": self.Image40,
      "41": self.Image41,
      "42": self.Image42,
      
      ///FL
      "0" : self.Image00,
      "1" : self.Image01,
      "2" : self.Image02,
      "3" : self.Image03,
      "4" : self.Image04,
      "5" : self.Image05,
      "6" : self.Image06,
      "7" : self.Image07,
      "8" : self.Image08,
      "9" : self.Image09,
      "10": self.Image10,
      "11": self.Image11,
      "12": self.Image12,
      "13": self.Image13,
      "14": self.Image14,
      "15": self.Image15,
      "16": self.Image16,
      "17": self.Image17,
      "18": self.Image18,
      "19": self.Image19,
      "20": self.Image20,
      "21": self.Image21,
      "22": self.Image22,
      "23": self.Image23,
      "24": self.Image24,
      "25": self.Image25,
      "26": self.Image26,
      "27": self.Image27,
      "28": self.Image28,
      "29": self.Image29,
      "30": self.Image30,
      "31": self.Image31,
      "32": self.Image32,
      "33": self.Image33,
      "34": self.Image34,
      
      ///BR
      "70": self.Image70,
      "71": self.Image71,
      "72": self.Image72,
      "73": self.Image73,
      "74": self.Image74,
      "75": self.Image75,
      "76": self.Image76,
      "77": self.Image77,
      "78": self.Image78,
      "79": self.Image79,
      "80": self.Image80,
      "81": self.Image81,
      "82": self.Image82,
      "83": self.Image83,
      "84": self.Image84,
      "85": self.Image85,
      "86": self.Image86,
      "87": self.Image87,
      "88": self.Image88,
      "89": self.Image89,
      "90": self.Image90,
      "91": self.Image91,
      "92": self.Image92,
      "93": self.Image93,
      "94": self.Image94,
      "95": self.Image95,
      "96": self.Image96,
      "97": self.Image97,
      "98": self.Image98,
      
      ///BL
      "123": self.Image123,
      "124": self.Image124,
      "125": self.Image125,
      "126": self.Image126,
      "127": self.Image127,
      "115": self.Image115,
      "116": self.Image116,
      "117": self.Image117,
      "118": self.Image118,
      "119": self.Image119,
      "120": self.Image120,
      "121": self.Image121,
      "122": self.Image122,
      "107": self.Image107,
      "108": self.Image108,
      "109": self.Image109,
      "110": self.Image110,
      "111": self.Image111,
      "112": self.Image112,
      "113": self.Image113,
      "114": self.Image114,
      "99" : self.Image99,
      "100": self.Image100,
      "101": self.Image101,
      "102": self.Image102,
      "103": self.Image103,
      "104": self.Image104,
      "105": self.Image105,
      "106": self.Image106
    ]
    
    circleViewsDictionary = [
      ///FR
      "65": self.Circle65,
      "66": self.Circle66,
      "67": self.Circle67,
      "68": self.Circle68,
      "69": self.Circle69,
      "60": self.Circle60,
      "61": self.Circle61,
      "62": self.Circle62,
      "63": self.Circle63,
      "64": self.Circle64,
      "51": self.Circle51,
      "52": self.Circle52,
      "53": self.Circle53,
      "54": self.Circle54,
      "55": self.Circle55,
      "56": self.Circle56,
      "57": self.Circle57,
      "58": self.Circle58,
      "59": self.Circle59,
      "43": self.Circle43,
      "44": self.Circle44,
      "45": self.Circle45,
      "46": self.Circle46,
      "47": self.Circle47,
      "48": self.Circle48,
      "49": self.Circle49,
      "50": self.Circle50,
      "35": self.Circle35,
      "36": self.Circle36,
      "37": self.Circle37,
      "38": self.Circle38,
      "39": self.Circle39,
      "40": self.Circle40,
      "41": self.Circle41,
      "42": self.Circle42,
      
      ///FL
      "0" : self.Circle00,
      "1" : self.Circle01,
      "2" : self.Circle02,
      "3" : self.Circle03,
      "4" : self.Circle04,
      "5" : self.Circle05,
      "6" : self.Circle06,
      "7" : self.Circle07,
      "8" : self.Circle08,
      "9" : self.Circle09,
      "10": self.Circle10,
      "11": self.Circle11,
      "12": self.Circle12,
      "13": self.Circle13,
      "14": self.Circle14,
      "15": self.Circle15,
      "16": self.Circle16,
      "17": self.Circle17,
      "18": self.Circle18,
      "19": self.Circle19,
      "20": self.Circle20,
      "21": self.Circle21,
      "22": self.Circle22,
      "23": self.Circle23,
      "24": self.Circle24,
      "25": self.Circle25,
      "26": self.Circle26,
      "27": self.Circle27,
      "28": self.Circle28,
      "29": self.Circle29,
      "30": self.Circle30,
      "31": self.Circle31,
      "32": self.Circle32,
      "33": self.Circle33,
      "34": self.Circle34,
      
      ///BR
      "70": self.Circle70,
      "71": self.Circle71,
      "72": self.Circle72,
      "73": self.Circle73,
      "74": self.Circle74,
      "75": self.Circle75,
      "76": self.Circle76,
      "77": self.Circle77,
      "78": self.Circle78,
      "79": self.Circle79,
      "80": self.Circle80,
      "81": self.Circle81,
      "82": self.Circle82,
      "83": self.Circle83,
      "84": self.Circle84,
      "85": self.Circle85,
      "86": self.Circle86,
      "87": self.Circle87,
      "88": self.Circle88,
      "89": self.Circle89,
      "90": self.Circle90,
      "91": self.Circle91,
      "92": self.Circle92,
      "93": self.Circle93,
      "94": self.Circle94,
      "95": self.Circle95,
      "96": self.Circle96,
      "97": self.Circle97,
      "98": self.Circle98,
      
      ///BL
      "123": self.Circle123,
      "124": self.Circle124,
      "125": self.Circle125,
      "126": self.Circle126,
      "127": self.Circle127,
      "115": self.Circle115,
      "116": self.Circle116,
      "117": self.Circle117,
      "118": self.Circle118,
      "119": self.Circle119,
      "120": self.Circle120,
      "121": self.Circle121,
      "122": self.Circle122,
      "107": self.Circle107,
      "108": self.Circle108,
      "109": self.Circle109,
      "110": self.Circle110,
      "111": self.Circle111,
      "112": self.Circle112,
      "113": self.Circle113,
      "114": self.Circle114,
      "99" : self.Circle99,
      "100": self.Circle100,
      "101": self.Circle101,
      "102": self.Circle102,
      "103": self.Circle103,
      "104": self.Circle104,
      "105": self.Circle105,
      "106": self.Circle106
    ]
  }
}
