//
//  ViewController+SplitScan.swift
//  ScannerAcorys
//
//  Created by Corify Care on 10/5/24.
//

import Foundation
import GLKit
import MetalKit
import Structure
import UIKit

extension ViewController {
  func setSnapshotsPath(snapshotsDir: String?) {
    listCameraPoses = ""
    listImages = ""
    snapshotsYaml = ""
    self.snapshotsDir = snapshotsDir!
  }
  
  func getMeshExportFormat() -> Int {
    // Get the format from settings.bundle
    let val = UserDefaults.standard.integer(forKey: "meshExportFormat")
    return val
  }
  
  func saveSplitScan(mesh: STMesh) {
    let fileManager = FileManager.default
    guard let dirs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
    let objFilename = "Model\(self.splitSection).zip"
    
    let mainPath = dirs.appendingPathComponent("\(self.mainScanDir)/\(self.scanDir)")
    let scanPath = mainPath.appendingPathComponent("\(self.scanDir)_\(self.splitSection)")
    
    do {
      try fileManager.createDirectory(at: mainPath, withIntermediateDirectories: true, attributes: nil)
      try fileManager.createDirectory(at: scanPath, withIntermediateDirectories: true, attributes: nil)
    } catch {
      print("Error creating directory: \(error)")
    }
    
    let exportExtensions: [Int: String] = [
      STMeshWriteOptionFileFormat.objFile.rawValue: "obj",
      STMeshWriteOptionFileFormat.objFileZip.rawValue: "zip",
      STMeshWriteOptionFileFormat.plyFile.rawValue: "ply",
      STMeshWriteOptionFileFormat.binarySTLFile.rawValue: "stl"
    ]
    
    guard let meshExportFormat = exportExtensions[self.getMeshExportFormat()] else {
      self.hideAppStatusMessage()
      return
    }
    
    let filename = String.localizedStringWithFormat("Model.%@", meshExportFormat)
    let filePath = scanPath.appendingPathComponent("\(filename)")
    
    let options: [AnyHashable: Any] = [
      kSTMeshWriteOptionFileFormatKey: self.getMeshExportFormat(),
      kSTMeshWriteOptionUseXRightYUpConventionKey: true
    ]
    
    do {
      try mesh.write(toFile: filePath.path, options: options)
    } catch {
      self.hideAppStatusMessage()
      self.showAlert(title: "ERROR!!!", message: "Mesh exporting failed: \(error.localizedDescription).")
      
      return
    }
    
    self.splitSection += 1
    
    if let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
      var snapshotsPath = docDir.appendingPathComponent("\(self.mainScanDir)/\(self.scanDir)/\(self.scanDir)_\(self.splitSection)/snapshots").path
      
      if !fileManager.fileExists(atPath: snapshotsPath) {
        do {
          try fileManager.createDirectory(atPath: snapshotsPath, withIntermediateDirectories: true, attributes: nil)
        } catch {
          print("Error creating directory: \(error)")
        }
      }
      
      onSLAMOptionsChanged()
      saveSnapshotsYaml()
      
      snapshotsPath = docDir.appendingPathComponent("\(self.mainScanDir)/\(self.scanDir)/\(self.scanDir)_\(self.splitSection)/snapshots").path
      
      if !fileManager.fileExists(atPath: snapshotsPath) {
        do {
          try fileManager.createDirectory(atPath: snapshotsPath, withIntermediateDirectories: true, attributes: nil)
        } catch {
          print("Error creating directory: \(error)")
        }
      }
      
      let snapshotsDir = "\(self.mainScanDir)/\(self.scanDir)/\(self.scanDir)_\(self.splitSection)/snapshots"
      setSnapshotsPath(snapshotsDir: snapshotsDir)
    }
    
    let newImageName = "\(self.splitSection).circle.fill"
    self.numberSplitImage.image = UIImage(systemName: newImageName)
    
    self.prepareForNextArucos()
    self.arucoDetectedParts.append(self.currentArucoDetected)
    self.currentArucoDetected = []
  }
  
  func splitMeshViewDidRequestColorizing(_ mesh: STMesh, previewCompletionHandler: @escaping () -> Void, enhancedCompletionHandler: @escaping () -> Void) -> Bool {
    if naiveColorizeTask != nil {
      print("Already one colorizing task running!")
      return false
    }
    
    do {
      naiveColorizeTask = try STColorizer.newColorizeTask(with: mesh, scene: slamState.scene, keyframes: slamState.keyFrameManager!.getKeyFrames(), completionHandler: { error in
        if error != nil {
          print("Error during colorizing: \(error!.localizedDescription)")
        } else {
          DispatchQueue.main.async {
            previewCompletionHandler()
            self.meshViewController?.mesh = mesh
            self.performEnhancedColorize(mesh, enhancedCompletionHandler: enhancedCompletionHandler)
          }
          self.naiveColorizeTask = nil
        }
      }, options: [
        kSTColorizerTypeKey: NSNumber(value: STColorizerType.perVertex.rawValue),
        kSTColorizerPrioritizeFirstFrameColorKey: NSNumber(value: options.prioritizeFirstFrameColor)
      ])
    } catch {}
    
    if naiveColorizeTask != nil {
      // Release the tracking and mapping resources. It will not be possible to resume a scan after this point
      slamState.mapper?.reset()
      slamState.tracker?.reset()
      naiveColorizeTask?.delegate = self
      naiveColorizeTask?.start()
      return true
    }
    
    return false
  }
  
  func splitPerformEnhancedColorize(_ mesh: STMesh, enhancedCompletionHandler: @escaping () -> Void) {
    do {
      enhancedColorizeTask = try STColorizer.newColorizeTask(with: mesh, scene: slamState.scene, keyframes: slamState.keyFrameManager!.getKeyFrames(), completionHandler: { error in
        if error != nil {
          print("Error during colorizing: \(error!.localizedDescription)")
        } else {
          DispatchQueue.main.async {
            enhancedCompletionHandler()
            self.meshViewController?.mesh = mesh
          }
          self.enhancedColorizeTask = nil
        }
      }, options: [
        kSTColorizerTypeKey: NSNumber(value: STColorizerType.textureMapForObject.rawValue),
        kSTColorizerPrioritizeFirstFrameColorKey: NSNumber(value: options.prioritizeFirstFrameColor),
        kSTColorizerQualityKey: NSNumber(value: options.colorizerQuality.rawValue),
        kSTColorizerTargetNumberOfFacesKey: NSNumber(value: options.colorizerTargetNumFaces)
      ] /* 20k faces is enough for most objects. */ )
    } catch {}
    
    if enhancedColorizeTask != nil {
      // We don't need the keyframes anymore now that the final colorizing task was started.
      // Clearing it now gives a chance to early release the keyframe memory when the colorizer
      // stops needing them.
      slamState.keyFrameManager!.clear()
      
      enhancedColorizeTask?.delegate = self
      enhancedColorizeTask?.start()
    }
  }
  
  func OLDsplitMeshViewDidRequestColorizing(_ mesh: STMesh, previewCompletionHandler: @escaping () -> Void, enhancedCompletionHandler: @escaping () -> Void) -> Bool {
    if naiveColorizeTask != nil {
      print("Already one colorizing task running!")
      hideAppStatusMessage()
      return false
    }
    
    do {
      naiveColorizeTask = try STColorizer.newColorizeTask(with: mesh, scene: slamState.scene, keyframes: slamState.keyFrameManager!.getKeyFrames(), completionHandler: { error in
        if let error = error {
          print("Error during colorizing: \(error.localizedDescription)")
        } else {
          DispatchQueue.main.async {
            previewCompletionHandler()
            self.splitPerformEnhancedColorize(mesh, enhancedCompletionHandler: enhancedCompletionHandler)
          }
          self.naiveColorizeTask = nil
        }
      }, options: [
        kSTColorizerTypeKey: NSNumber(value: STColorizerType.perVertex.rawValue),
        kSTColorizerPrioritizeFirstFrameColorKey: NSNumber(value: options.prioritizeFirstFrameColor)
      ])
    } catch {}
    
    if naiveColorizeTask != nil {
      slamState.mapper?.reset()
      slamState.tracker?.reset()
      naiveColorizeTask?.delegate = self
      naiveColorizeTask?.start()
      return true
    }
    
    hideAppStatusMessage()
    return false
  }
  
  func OLDsplitPerformEnhancedColorize(_ mesh: STMesh, enhancedCompletionHandler: @escaping () -> Void) {
    do {
      enhancedColorizeTask = try STColorizer.newColorizeTask(with: mesh, scene: slamState.scene, keyframes: slamState.keyFrameManager!.getKeyFrames(), completionHandler: { error in
        if error != nil {
          print("Error during colorizing: \(error!.localizedDescription)")
        } else {
          DispatchQueue.main.async {
            enhancedCompletionHandler()
            self.meshViewController?.mesh = mesh
          }
          self.enhancedColorizeTask = nil
        }
      }, options: [
        kSTColorizerTypeKey: NSNumber(value: STColorizerType.textureMapForObject.rawValue),
        kSTColorizerPrioritizeFirstFrameColorKey: NSNumber(value: options.prioritizeFirstFrameColor),
        kSTColorizerQualityKey: NSNumber(value: options.colorizerQuality.rawValue),
        kSTColorizerTargetNumberOfFacesKey: NSNumber(value: options.colorizerTargetNumFaces)
      ] /* 20k faces is enough for most objects. */ )
    } catch {}
    
    if enhancedColorizeTask != nil {
      // We don't need the keyframes anymore now that the final colorizing task was started.
      // Clearing it now gives a chance to early release the keyframe memory when the colorizer
      // stops needing them.
      slamState.keyFrameManager!.clear()
      
      enhancedColorizeTask?.delegate = self
      enhancedColorizeTask?.start()
    }
  }
  
}
