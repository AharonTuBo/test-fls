//
//  OpenCVWrapper.h
//  ScannerAcorys
//
//  Created by Corify Care on 8/5/24.
//

#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>
#import <SceneKit/SceneKit.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ArucoCV : NSObject

+(NSMutableArray *) estimatePose:(CVPixelBufferRef)pixelBuffer withIntrinsics:(matrix_float3x3)intrinsics andMarkerSize:(Float64)markerSize;
+(NSMutableArray *) estimatePose:(CVPixelBufferRef)pixelBuffer andMarkerSize:(Float64)markerSize;
+(NSArray*) detectArucos:(UIImage *)image;
@end

NS_ASSUME_NONNULL_END
